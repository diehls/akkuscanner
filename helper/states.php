<?php
abstract class States{
    const FAIL = 0;
    const SUCCESS = 1;
    const USER_EXIST = 2;
    const USER_NOT_EXIST = 3;
    const AKKU_EXIST = 4;
    const AKKU_NOT_EXIST = 5;
    const USER_HAS_NO_AKKUS = 6;
    const NOT_ALL_PARAMS = 7;
}