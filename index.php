<?php

include_once __DIR__ . '/helper/include.php';


if(USE_HTTPS){
    if($_SERVER['HTTPS'] != 'on'){
        header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        exit();
    }
}


if (isset($_SESSION['id'])) {
    header("Location: ".HOME_DIRECTORY."/appLogic/dataTable.php");
}


?>

<!DOCTYPE html>
<html lang="de" >

<head>
    <meta charset="UTF-8">
    <title>AkkuCheck</title>

    <link rel="icon" href="Icon.png">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>

<div class="login-page">
    <div class="form">
        <form class="register-form" action=<?php echo HOME_DIRECTORY."/login_signup/signup.php" ?> method="post">
            <input type="text" placeholder="Name" name="name" required/>
            <?php
            if (isset($_GET['error'])){
                if ($_GET['error'] == "USER_EXISTS"){
                    ?>
                    <output class="errorMessage">Die Email-Adresse wird bereits verwendent</output>
                    <?php
                }
            }
            ?>
            <input type="text" placeholder="Email-Adresse" name="emailAddress" required/>
            <input type="password" placeholder="Passwort" name="password" required/>
            <button>create</button>
            <p class="message">Already registered? <a href="#">Sign In</a></p>
        </form>
        <form class="login-form" action=<?php echo HOME_DIRECTORY."/login_signup/login.php" ?> method="post" >
            <?php
            if (isset($_GET['error'])){
                if ($_GET['error'] == "LOGIN_INCORRECT"){
                    ?>
                    <output class="errorMessage">Die Login Daten sind inkorrekt</output>
                    <?php
                }
            }
            ?>
            <input type="text" placeholder="Email-Adresse" name="emailAddress" required/>
            <input type="password" placeholder="Passwort" name="password" required/>
            <button>login</button>
            <p class="message">Not registered? <a href="#">Create an account</a></p>
        </form>
    </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<?php
    if (isset($_GET['error'])){
        if ($_GET['error'] == "USER_EXISTS"){

            ?>
            <script>
                $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
            </script>

<?php
        }
    }
?>


<script  src="js/index.js"></script>

</body>

