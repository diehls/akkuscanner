<?php


include_once '../api/config/Database.php';
include_once '../api/objects/User.php';
include_once __DIR__ . '/../helper/include.php';

$email = $_POST['emailAddress'];
$password = $_POST['password'];
$hashedPassword = sha1($password);

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

$user->email = $email;
$user->password = $hashedPassword;

$result = $user->authenticate();



if ($result == States::SUCCESS) {
    $_SESSION['id'] = $user->id;
    header("Location: ".HOME_DIRECTORY."/appLogic/dataTable.php");
} else {
    header("Location: ".HOME_DIRECTORY."/index.php?error=LOGIN_INCORRECT");

}