<?php

include_once '../api/config/Database.php';
include_once '../api/objects/User.php';
include_once __DIR__ . '/../helper/include.php';



$email = $_POST['emailAddress'];
$name = $_POST['name'];
$password = $_POST['password'];
$hashedPassword = sha1($password);

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

$user->email = $email;
$user->username = $name;
$user->password = $hashedPassword;

$result = $user->create();


if ($result == States::SUCCESS){
    $_SESSION['id'] = $user->id;
    header("Location: ".HOME_DIRECTORY."/appLogic/dataTable.php");

}elseif  ($result == States::USER_EXIST){
    header("Location: ".HOME_DIRECTORY."/index.php?error=USER_EXISTS");
}else {
    echo 'fail';
}
