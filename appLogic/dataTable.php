<?php

require_once __DIR__ . '/../Composer/vendor/autoload.php';

include_once __DIR__.'/../api/config/Database.php';
include_once __DIR__.'/../api/objects/Akku.php';
include_once __DIR__ . '/../helper/include.php';


class Main{

    private $userID;
    private $akku_arr;

    /**
     * Main constructor.
     */
    public function __construct()
    {

        $this->userID = $_SESSION['id'];


        $database = new Database();
        $db = $database->getConnection();

        $akku = new Akku($db);

        $akku->user_id = $this->userID;

        $stmt = $akku->read_user_akkus();
        $num = $stmt->rowCount();

        if($num>0){
            $user_arr = array();

            $id = null;
            $accu_name = null;
            $accu_loads = null;
            $user_id = null;

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $user_item = array(
                    "id" => $id,
                    "name" => $accu_name,
                    "loads" => $accu_loads,
                    "user_id" => $user_id,
                );

                array_push($user_arr, $user_item);
            }

            $this->akku_arr = $user_arr;
        }else {
            echo "You have no Akkus";
            exit();
        }
    }

    function print_array_to_table(){
        foreach ($this->akku_arr as $item1 => $name1):
            foreach ($name1 as $item2 => $name2):
                if ($item2 === "name" ){
                    echo '<tr>';
                    echo '<td>' .$name2  .'</td>';
                }elseif($item2 === "loads"){
                    echo '<td>' . $name2  .'</td>';
                    echo '</tr>';
                }
            endforeach;
        endforeach;
    }

}

$m = new Main();

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Akku Tabelle</title>
    <link rel="icon" href=<?php echo HOME_DIRECTORY."/Icon.png" ?>>
    <link rel="stylesheet" href=<?php echo HOME_DIRECTORY."/css/table.css" ?> type="text/css">

</head>
<body bgcolor="#76b852">
<div class="container">
    <table class="mainTable">
        <thead>
        <tr>
            <th>Akku Code</th>
            <th>Ladungen</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $m->print_array_to_table();
        ?>
        </tbody>
    </table>
</div>
</body>
</html>



