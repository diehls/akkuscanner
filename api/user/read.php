<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/Database.php';
include_once '../objects/User.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

$stmt = $user->read();

if(is_a($stmt, "PDOException")){
    echo json_encode(array("error" => $stmt->getMessage()));
    exit;
}


$num = $stmt->rowCount();


if($num>0){
    $user_arr=array();

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
        $user_item=array(
            "id" => $id,
            "username" => $username,
            "email" => $email
        );

        array_push($user_arr, $user_item);
    }
    echo json_encode($user_arr);
}else {
    echo json_encode(array("message" => "no products found"));
}