<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/Database.php';
include_once '../objects/User.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

$data = json_decode(file_get_contents("php://input"));

if($data->key != AUTH_KEY){
    echo '{';
    echo '"error": "Key Incorrect"';
    echo '}';
    exit;
}

$username = $data->username;
$email = $data->email;
$password = $data->password;
$hashedPassword = sha1($password);

$result = $user->create($username, $email, $hashedPassword);

if(is_a($result, "PDOException")){
    echo json_encode(array("error" => $result->getMessage()));
    exit;
}


if($result == States::USER_EXIST){
    echo '{';
    echo '"message": "Unable to create user : User already exists"';
    echo '}';
}elseif ($result == States::SUCCESS) {

    $user->readByEmail();
    echo '{';
    echo '"message": "User was created",';
    echo '"id" : ' . $user->id;
    echo '}';

}elseif($result == States::NOT_ALL_PARAMS){
    echo '{';
    echo '"message": "Unable to create user : Not all parameter are given"';
    echo '}';
}else{
    echo '{';
    echo '"message": "Unable to create user"';
    echo '}';
}

