<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/Database.php';
include_once '../objects/User.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

$data = json_decode(file_get_contents("php://input"));

if($data->key != AUTH_KEY){
    echo '{';
    echo '"error": "Key Incorrect"';
    echo '}';
    exit;
}

$user->id = $data->id;


if($user->delete()){
    echo '{';
        echo '"message": "User was deleted."';
    echo '}';
}

// if unable to delete the product
else{
    echo '{';
        echo '"message": "Unable to delete user."';
    echo '}';
}