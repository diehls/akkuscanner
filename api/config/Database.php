<?php
/**
 * Created by PhpStorm.
 * User: lenny
 * Date: 08.01.2018
 * Time: 13:29
 */
include_once __DIR__.'/../../helper/include.php';


class Database{
 
    private $host = "";
    private $db_name = "";
    private $username = "";
    private $password = "";
    public $conn;

    /**
     *
     */
    public function getConnection(){
        $this->host = DB_CONFIG_HOST;
        $this->db_name = DB_CONFIG_DBNAME;
        $this->username = DB_CONFIG_USERNAME;
        $this->password = DB_CONFIG_PW;
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->exec("set names utf8");
        }catch (PDOException $exception){
            echo "Connection error: " .$exception->getMessage();
        }

        return $this->conn;
    }
}