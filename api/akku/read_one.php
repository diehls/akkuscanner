<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../config/Database.php';
include_once '../objects/Akku.php';

$database = new Database();
$db = $database->getConnection();

$akku = new Akku($db);

$akku->id = isset($_GET['id']) ? $_GET['id'] : die();

$akku->read_one();

$user_arr = array(
    "id" => $akku->id,
    "name" => $akku->name,
    "loads" => $akku->loads,
    "user_id" => $akku->user_id,
    "dates" => $akku->date
);

print_r(json_encode($user_arr));