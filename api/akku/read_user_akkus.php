<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../config/Database.php';
include_once '../objects/Akku.php';

$database = new Database();
$db = $database->getConnection();

$akku = new Akku($db);

$akku->user_id = isset($_GET['user_id']) ? $_GET['user_id'] : die();

$stmt = $akku->read_user_akkus();
$num = $stmt->rowCount();

if($num>0){
    $user_arr = array();

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $user_item = array(
            "id" => $id,
            "name" => $accu_name,
            "loads" => $accu_loads,
            "user_id" => $user_id,
        );

        array_push($user_arr, $user_item);
    }

    print_r(json_encode($user_arr));
}else {
    echo json_encode(array("message" => "no products found"));
}


