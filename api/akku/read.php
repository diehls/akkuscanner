<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/Database.php';
include_once '../objects/Akku.php';

$database = new Database();
$db = $database->getConnection();

$akku = new Akku($db);

$stmt = $akku->read();
$num = $stmt->rowCount();

if($num>0){
    $akku_arr=array();


    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);
        $akku_item=array(
            "id" => $id,
            "name" => $accu_name,
            "loads" => $accu_loads,
            "user_id" => $user_id
        );

        array_push($akku_arr, $akku_item);
    }
    echo json_encode($akku_arr);
}else {
    echo json_encode(array("message" => "no products found"));
}