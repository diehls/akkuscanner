<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/Database.php';
include_once '../objects/Akku.php';
include_once '../../helper/states.php';

$database = new Database();
$db = $database->getConnection();

$akku = new Akku($db);

$data = json_decode(file_get_contents("php://input"));

if($data->key != AUTH_KEY){
    echo '{';
    echo '"error": "Key Incorrect"';
    echo '}';
    exit;
}

$akku->name = $data->name;
$akku->id = $data->id;
$result = $akku->load();



if($result == States::SUCCESS){
    echo '{';
    echo '"message": "Akku was loaded.",';
    echo '"loads" : '.$akku->loads;
    echo '}';
}else {
    echo '{';
    echo '"message": "Unable to load akku."';
    echo '}';
}
