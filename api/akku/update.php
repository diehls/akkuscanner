<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/Database.php';
include_once '../objects/Akku.php';

$database = new Database();
$db = $database->getConnection();

$akku = new Akku($db);

$data = json_decode(file_get_contents("php://input"));

if($data->key != AUTH_KEY){
    echo '{';
    echo '"error": "Key Incorrect"';
    echo '}';
    exit;
}

$akku->id = $data->id;

$akku->name = $data->name;
$akku->loads = $data->loads;
$akku->user_id = $data->user_id;

if($akku->update()){
    echo '{';
    echo '"message": "Akku was updated."';
    echo '}';
}

// if unable to update the product, tell the user
else{
    echo '{';
    echo '"message": "Unable to update akku."';
    echo '}';
}