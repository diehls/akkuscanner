<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/Database.php';
include_once '../objects/Akku.php';
include_once '../../helper/states.php';
include_once '../../helper/include.php';

$database = new Database();
$db = $database->getConnection();

$akku = new Akku($db);

$data = json_decode(file_get_contents("php://input"));

if($data->key != AUTH_KEY){
    echo '{';
    echo '"error": "Key Incorrect"';
    echo '}';
    exit;
}

$akku->name = $data->name;
$akku->loads = 0;
$akku->user_id = $data->user_id;

$result = $akku->create();

if($result == States::SUCCESS){
    $akku->load();
    echo '{';
    echo '"message": "Akku was created.",';
    echo '"id" : '.$akku->id;
    echo '}';


}elseif($result == States::AKKU_NOT_EXIST){
    echo '{';
    echo '"message": "Unable to create akku : Akku Already Exists."';
    echo '}';
}else {
    echo '{';
    echo '"message": "Unable to create akku."';
    echo '}';
}
