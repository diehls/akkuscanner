<?php


include_once __DIR__."/../../helper/states.php";

class User
{
    private $conn;

    public $id;
    public $username;
    public $email;
    public $password;

    /**
     * User constructor.
     * @param $db
     */
    public function __construct($db){
        $this->conn = $db;
    }

    /**
     * @return mixed
     */
    function read(){
        try {
            $query = "SELECT users.id, users.username, users.email FROM users ";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }catch (PDOException $e){
            return $e;
        }


    }

    /**
     * @param $email
     * @param $username
     * @param $password
     * @return bool
     */
    function create($email, $username, $password){
        if (isset($email) && isset($username) && isset($password)){
            if($this->userExists()){
                return STATES::USER_EXIST;
            }else {
                try {
                    $query = "INSERT INTO users SET email = :email, username = :username, password = :password";

                    $stmt = $this->conn->prepare($query);

                    $this->email=htmlspecialchars(strip_tags($email));
                    $this->username=htmlspecialchars(strip_tags($username));
                    $this->password=htmlspecialchars(strip_tags($password));

                    $stmt->bindParam(":email", $this->email);
                    $stmt->bindParam(":username", $this->username);
                    $stmt->bindParam(":password", $this->password);

                    if($stmt->execute()){
                        return States::SUCCESS;
                    }else {
                        return States::FAIL;
                    }

                }catch (PDOException $e){
                    return $e;
                }
            }
        }else {
            return States::NOT_ALL_PARAMS;
        }


    }

    /**
     *
     */
    function readOne(){
        $query = "SELECT users.id, users.username, users.email FROM users WHERE users.id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->email = $row['email'];
        $this->username = $row['username'];
        $this->id = $row['id'];
    }

    /**
     *
     */
    function readByEmail(){
        $query = "SELECT users.id FROM users WHERE users.email = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->email);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $row['id'];
    }

    /**
     * @return bool
     */
    function update(){
        $query = "UPDATE users SET email = :email, username = :username, password = :password   WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->password = htmlspecialchars(strip_tags($this->password));

        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':id', $this->id);

        if($stmt->execute()){
            return true;
        }
        return false;

    }

    /**
     * @return int
     */
    function authenticate(){
        if($this->userExists()){
            $statement = $this->conn->prepare("SELECT * FROM users WHERE email = ? AND password = ?");

            $this->email = htmlspecialchars(strip_tags($this->email));
            $this->password = htmlspecialchars(strip_tags($this->password));

            $statement->bindParam(1, $this->email);
            $statement->bindParam(2, $this->password);
            $statement->execute();
            $row = $statement->fetch();

            if ($statement->rowCount() > 0){
                $this->id = $row['id'];
                $this->email = $row['email'];
                $this->username = $row['username'];
                return States::SUCCESS;
            }else {
                return States::USER_NOT_EXIST;
            }
        }else {
            return States::USER_NOT_EXIST;
        }
    }

    /**
     * @return bool
     */
    function delete(){
        $query = "DELETE FROM users WHERE id = ?";

        $stmt = $this->conn->prepare($query);

        $this->id=htmlspecialchars(strip_tags($this->id));

        $stmt->bindParam(1, $this->id);

        if($stmt->execute()){
            return true;
        }

        return false;

    }

    /**
     * @return bool
     */
    function userExists(){
        $query = $this->conn->prepare("SELECT email FROM users WHERE email = ?");
        $query->bindValue(1, $this->email);
        $query->execute();

        if( $query->rowCount() > 0 ) { # If rows are found for query
            return true;
        }
        else {
            return false;
        }
    }

}