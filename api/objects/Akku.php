<?php
/**
 * Created by PhpStorm.
 * User: lenny
 * Date: 08.01.2018
 * Time: 16:55
 */

include_once __DIR__."/../../helper/states.php";
include_once __DIR__ . '/../../helper/include.php';


class Akku
{
    private $conn;

    public $id;
    public $name;
    public $loads;
    public $user_id;
    public $date;
    public $akku_arr;

    /**
     * Akku constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->conn = $db;
    }

    /**
     * @return mixed
     */
    function read(){
        $query = "SELECT
                    accu_loads.id,
                    accu_loads.accu_name,
                    accu_loads.accu_loads,
                    accu_loads.user_id
                FROM
                    accu_loads
                ORDER BY
                    accu_loads.id ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    /**
     * @return int
     */
    function create(){
        if($this->akkuExists()){
            return States::AKKU_NOT_EXIST;
        }else {
            $query = "INSERT INTO accu_loads SET accu_name = :name, accu_loads = :loads, user_id = :user_id";

            $stmt = $this->conn->prepare($query);

            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->loads=htmlspecialchars(strip_tags($this->loads));
            $this->user_id=htmlspecialchars(strip_tags($this->user_id));


            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":loads", $this->loads);
            $stmt->bindParam(":user_id", $this->user_id);

            if($stmt->execute()){
                return States::SUCCESS;
            }
            return States::FAIL;
        }
    }

    /**
     * @return int
     */
    function load(){
        if($this->akkuExists()){
            if (!isset($this->id)){
                $this->getIDbyName();
            }

            $getAkku = $this->conn->prepare("SELECT accu_loads FROM accu_loads WHERE id = ?");
            $getAkku->bindValue(1, $this->id);
            $getAkku->execute();
            $data = $getAkku->fetch();
            $this->loads = $data['accu_loads'];

            $this->loads =  $this->loads + 1;


            $update = $this->conn->prepare("UPDATE accu_loads SET accu_loads = ? WHERE id = ?");
            $update->bindValue(1,  $this->loads);
            $update->bindValue(2, $this->id);
            $update->execute();


            date_default_timezone_set('Europe/Berlin');
            $date = date('Y-d-m H:i:s');

            $setDate = $this->conn->prepare("INSERT INTO accu_dates (accu_loads_id, accu_load_date) VALUES (?,?)");
            $setDate->execute((array($this->id, $date)));
            return States::SUCCESS;
        }else {
            return States::AKKU_NOT_EXIST;
        }

    }

    /**
     *
     */
    function read_one(){
        $query = "SELECT
                    accu_loads.id,
                    accu_loads.accu_name,
                    accu_loads.accu_loads,
                    accu_loads.user_id,
                    accu_dates.accu_load_date
                FROM
                    accu_loads,
                    accu_dates 
                WHERE
                    accu_loads.id = accu_dates.accu_loads_id
                    AND accu_loads.id = ?
                ORDER BY
                    accu_loads.id";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $date_arr = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->name = $row['accu_name'];
            $this->loads = $row['accu_loads'];
            $this->user_id = $row['user_id'];
            array_push($date_arr, $row['accu_load_date']);
        }
        $this->date = $date_arr;
    }

    /**
     * @return mixed
     */
    function read_user_akkus(){
        $query = "SELECT
                    accu_loads.id,
                    accu_loads.accu_name,
                    accu_loads.accu_loads,
                    accu_loads.user_id
      
                FROM
                    accu_loads
                WHERE
                    accu_loads.user_id = ?
                ORDER BY
                    accu_loads.id";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->user_id);
        $stmt->execute();

        return $stmt;
    }

    /**
     * @return bool
     */
    function update(){
        $query = "UPDATE accu_loads SET accu_name = :name, accu_loads = :loads, user_id = :user_id   WHERE id = :id";
        $stmt = $this->conn->prepare($query);

        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->loads = htmlspecialchars(strip_tags($this->loads));
        $this->user_id = htmlspecialchars(strip_tags($this->user_id));

        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':loads', $this->loads);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->bindParam(':id', $this->id);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    function delete(){
        $query = "DELETE FROM accu_loads WHERE id = ?";
        $query2 = "DELETE FROM accu_dates WHERE accu_loads_id = ?";

        $stmt = $this->conn->prepare($query);
        $stmt2 = $this->conn->prepare($query2);

        $this->id=htmlspecialchars(strip_tags($this->id));

        $stmt->bindParam(1, $this->id);
        $stmt2->bindParam(1, $this->id);

        if($stmt->execute()){
            if($stmt2->execute()){
                return true;
            }else {
                return false;
            }
        }
        return false;
    }

    /**
     *
     */
    private function getIDbyName(){
        $query = "SELECT accu_loads.id FROM accu_loads WHERE accu_loads.accu_name = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->name);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $row['id'];
    }

    /**
     * @return bool
     */
    public function akkuExists(){
        $query = $this->conn->prepare("SELECT accu_name FROM accu_loads WHERE accu_name = ?");
        $query->bindValue(1, $this->name);
        $query->execute();

        if( $query->rowCount() > 0 ) { # If rows are found for query
            return true;
        }
        else {
            return false;
        }
    }

}